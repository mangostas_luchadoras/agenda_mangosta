DROP DATABASE IF EXISTS `grupo_3`;
CREATE DATABASE `grupo_3`;
USE grupo_3;

CREATE TABLE `localidad`
(
  `idLocalidad` int(11) NOT NULL AUTO_INCREMENT,
  `Pais` varchar(45) NOT NULL,
  `Provincia` varchar(45) NOT NULL,
  `Ciudad` varchar(45) NOT NULL,
  `CP` varchar(10) NOT NULL,
  PRIMARY KEY (`idLocalidad`, `Pais`, `Provincia`, `Ciudad`, `CP`)
);

CREATE TABLE `tipo_contacto`
(
  `idTipo` int(11) NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(20) NOT NULL,
  PRIMARY KEY (`idTipo`)
);

CREATE TABLE `equipo`
(
  `idEquipo` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idEquipo`)
);

CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(25) NOT NULL,
  `Email` varchar(45) NOT NULL,
  `Cumpleanio` DATE NOT NULL,
  `idLocalidad` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `idEquipo` int(11) NOT NULL,
  PRIMARY KEY (`idPersona`),
  
  INDEX (idLocalidad),
  INDEX (idTipo),
  INDEX (idEquipo),
    
  FOREIGN KEY (idLocalidad)
  	REFERENCES localidad(idLocalidad)
    ON UPDATE CASCADE,
    
  FOREIGN KEY (idTipo)
  	REFERENCES tipo_contacto(idTipo)
    ON UPDATE CASCADE,
    
  FOREIGN KEY (idEquipo)
  	REFERENCES equipo(idEquipo)
    ON UPDATE CASCADE
);

CREATE TABLE `domicilio`
(
  `idDomicilio` int(11) NOT NULL,
  `Calle` varchar(45),
  `Numero` varchar(10),
  `Piso` varchar(10),
  `Dpto` varchar(10),
  PRIMARY KEY (`idDomicilio`, `Calle`, `Numero`, `Piso`, `Dpto`),
  
  INDEX (idDomicilio),
  FOREIGN KEY (idDomicilio)
  	REFERENCES personas(idPersona)
    ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO localidad VALUES(null, 'Argentina', 'Buenos Aires', 'Jose C Paz', '1665');
INSERT INTO localidad VALUES(null, 'Argentina', 'Buenos Aires', 'San Miguel', '1663');
INSERT INTO localidad VALUES(null, 'Argentina', 'Cordoba', 'Rio Cuarto', '1234');
INSERT INTO localidad VALUES(null, 'España', 'Andalucia', 'Cordoba', '5678');

INSERT INTO tipo_contacto VALUES(null, 'Amigo');
INSERT INTO tipo_contacto VALUES(null, 'Familia');
INSERT INTO tipo_contacto VALUES(null, 'Trabajo');
INSERT INTO tipo_contacto VALUES(null, 'Estudio');
INSERT INTO tipo_contacto VALUES(null, 'Club');

INSERT INTO equipo VALUES(null, 'Boca');
INSERT INTO equipo VALUES(null, 'River');
INSERT INTO equipo VALUES(null, 'Racing');
INSERT INTO equipo VALUES(null, 'Huracan');
INSERT INTO equipo VALUES(null, 'San Miguel');

INSERT INTO personas VALUES(null, 'Maximiliano Cosenza', '1145267894', 'mcosenza@gmail.com', '2020-11-20', 2, 3, 2);
INSERT INTO personas VALUES(null, 'Miguel Sulle', '1175369514', 'msulle@live.com', '2020-04-03', 1, 2, 1);
INSERT INTO personas VALUES(null, 'Gonzalo Gongora', '1184569312', 'ggongora@yahoo.com', '2020-05-13', 3, 1, 3);
INSERT INTO personas VALUES(null, 'Noelia Martinez', '1168425873', 'nvmartinez@gmail.com', '2020-10-30', 4, 4, 4);
INSERT INTO personas VALUES(null, 'Cristian Jonsson', '02320493168', 'cjonsson@hotmail.com.ar', '2020-10-30', 4, 5, 5);

INSERT INTO domicilio VALUES(1, 'Vicente Lopez', '2015', '2', 'A');
INSERT INTO domicilio VALUES(2, 'Urquiza', '1750', '', '');
INSERT INTO domicilio VALUES(3, 'Monteagudo', '4893', '5', 'D');
INSERT INTO domicilio VALUES(4, 'Cervantes', '1212', '', '');
INSERT INTO domicilio VALUES(5, 'Chile', '3662', '2', 'A');
