package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modelo.Agenda;
import persistencia.conexion.Conexion;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.LogIn;
import presentacion.vista.VentanaEdicion;
import presentacion.vista.VentanaEquipo;
import presentacion.vista.VentanaLocalidad;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaTipoContacto;
import presentacion.vista.Vista;
import dto.DomicilioDTO;
import dto.EquipoDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public class Controlador implements ActionListener
{
		private LogIn acceso;
		private Vista vista;
		private List<PersonaDTO> personasEnTabla;
		private List<String> paises;
		private List<String> provincias;
		private List<String> ciudades;
		private List<String> cp;
		private List<String> tipos;
		private List<String> equipos;
		private VentanaPersona ventanaPersona;
		private VentanaEdicion ventanaEdicion;
		private VentanaLocalidad ventanaLocalidad;
		private VentanaTipoContacto ventanaTipoContacto;
		private VentanaEquipo ventanaEquipo;
		private Agenda agenda;
		private int editable;
		PersonaDTO editablePersona;
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.acceso = LogIn.getInstance();
			this.acceso.getBtnEntrar().addActionListener(e->entrar(e));
			agregarEntersAcceso();
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(a->ventanaAgregarPersona(a));
			this.vista.getBtnEditar().addActionListener(e->ventanaEditarPersona(e));
			this.vista.getBtnBorrar().addActionListener(s->borrarPersona(s));
			this.vista.getBtnNuevaLocalidad().addActionListener(l->agregarLocalidad(l));
			this.vista.getBtnNuevoEquipo().addActionListener(eq->agregarEquipo(eq));
			this.vista.getBtnNuevoTipoContacto().addActionListener(t->agregarTipoContacto(t));
			this.vista.getBtnReporte().addActionListener(r->mostrarReporte(r));
			this.ventanaPersona = VentanaPersona.getInstance();
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));
			this.ventanaPersona.getBtnCancelar().addActionListener(c->cancelar(c));
			agregarEntersPersona();
			this.ventanaEdicion = VentanaEdicion.getInstance();
			this.ventanaEdicion.getBtnEditarPersona().addActionListener(ep->editarPersona(ep));
			this.ventanaEdicion.getBtnCancelar().addActionListener(ce->cancelarEdicion(ce));
			agregarEntersEdicion();
			this.ventanaLocalidad = VentanaLocalidad.getInstance();
			this.ventanaLocalidad.getBtnAgregarLocalidad().addActionListener(gl->guardarLocalidad(gl));
			this.ventanaLocalidad.getBtnCancelar().addActionListener(cl->cancelarLocalidad(cl));
			agregarEntersLocalidad();
			this.ventanaTipoContacto = VentanaTipoContacto.getInstance();
			this.ventanaTipoContacto.getBtnAgregarTipoContacto().addActionListener(gt->guardarTipoContacto(gt));
			this.ventanaTipoContacto.getBtnCancelar().addActionListener(ct->cancelarTipoContacto(ct));
			agregarEntersTipoContacto();			
			this.ventanaEquipo = VentanaEquipo.getInstance();
			this.ventanaEquipo.getBtnAgregarEquipo().addActionListener(ge->guardarEquipo(ge));
			this.ventanaEquipo.getBtnCancelar().addActionListener(ce->cancelarEquipo(ce));
			agregarEntersEquipo();
			this.agenda = agenda;
			this.editable = -1;
		}
			
		private void agregarEntersAcceso()
		{
			for (JTextField j : this.acceso.getTxtVentana())
			{
				j.addKeyListener(new KeyListener() {
			        @Override
			        public void keyTyped(KeyEvent e) {
			        }
			        @Override
			        public void keyPressed(KeyEvent e) {
			        	if(e.getKeyCode() == KeyEvent.VK_ENTER)
			        		entrar();		        		
			        }
			        @Override
			        public void keyReleased(KeyEvent e) {
			        }
				});
			}
		}
		
		private void agregarEntersPersona()
		{
			for (JTextField j : this.ventanaPersona.getTxtVentana())
			{
				j.addKeyListener(new KeyListener() {
			        @Override
			        public void keyTyped(KeyEvent e) {
			        }
			        @Override
			        public void keyPressed(KeyEvent e) {
			        	if(e.getKeyCode() == KeyEvent.VK_ENTER)
			        		guardarPersona();		        		
			        }
			        @Override
			        public void keyReleased(KeyEvent e) {
			        }
				});
			}
		}
		
		private void agregarEntersEdicion()
		{
			for (JTextField j : this.ventanaEdicion.getTxtVentana())
			{
				j.addKeyListener(new KeyListener() {
			        @Override
			        public void keyTyped(KeyEvent e) {
			        }
			        @Override
			        public void keyPressed(KeyEvent e) {
			        	if(e.getKeyCode() == KeyEvent.VK_ENTER)
			        		editarPersona();		        		
			        }
			        @Override
			        public void keyReleased(KeyEvent e) {
			        }
				});
			}
		}
		
		private void agregarEntersLocalidad()
		{
			for (JTextField j : this.ventanaLocalidad.getTxtVentana())
			{
				j.addKeyListener(new KeyListener() {
			        @Override
			        public void keyTyped(KeyEvent e) {
			        }
			        @Override
			        public void keyPressed(KeyEvent e) {
			        	if(e.getKeyCode() == KeyEvent.VK_ENTER)
			        		guardarLocalidad();		        		
			        }
			        @Override
			        public void keyReleased(KeyEvent e) {
			        }
				});
			}
		}
		
		private void agregarEntersTipoContacto()
		{
			this.ventanaTipoContacto.getTxtTipoContacto().addKeyListener(new KeyListener() {
				@Override
			    public void keyTyped(KeyEvent e) {
			    }
			    @Override
			    public void keyPressed(KeyEvent e) {
			    if(e.getKeyCode() == KeyEvent.VK_ENTER)
			    	guardarTipoContacto();		        		
			    }
			    @Override
			    public void keyReleased(KeyEvent e) {
			    }
			});
		}
		
		private void agregarEntersEquipo()
		{
			this.ventanaEquipo.getTxtEquipo().addKeyListener(new KeyListener() {
				@Override
			    public void keyTyped(KeyEvent e) {
			    }
			    @Override
			    public void keyPressed(KeyEvent e) {
			    if(e.getKeyCode() == KeyEvent.VK_ENTER)
			    	guardarEquipo();		        		
			    }
			    @Override
			    public void keyReleased(KeyEvent e) {
			    }
			});
		}
		
		private void agregarEntersComboPersona()
		{
			for (JComboBox<String> i : this.ventanaPersona.getComboBoxs())
			{
				KeyListener [] l = i.getKeyListeners();
				for (int j=0; j<l.length; j++)
					i.removeKeyListener(l[j]);
				{
					i.addKeyListener(new KeyListener() {
				        @Override
				        public void keyTyped(KeyEvent e) {
				        }
				        @Override
				        public void keyPressed(KeyEvent e) {
				        	if(e.getKeyCode() == KeyEvent.VK_ENTER)
				        		guardarPersona();		        		
				        }
				        @Override
				        public void keyReleased(KeyEvent e) {
				        }
					});
				}
			}
		}
		
		private void agregarEntersComboEdicion()
		{
			for (JComboBox<String> i : this.ventanaEdicion.getComboBoxs())
			{
				KeyListener [] l = i.getKeyListeners();
				for (int j=0; j<l.length; j++)
					i.removeKeyListener(l[j]);
				{
					i.addKeyListener(new KeyListener() {
				        @Override
				        public void keyTyped(KeyEvent e) {
				        }
				        @Override
				        public void keyPressed(KeyEvent e) {
				        	if(e.getKeyCode() == KeyEvent.VK_ENTER)
				        		editarPersona();		        		
				        }
				        @Override
				        public void keyReleased(KeyEvent e) {
				        }
					});
				}
			}
		}
		
		private void entrar(ActionEvent e)
		{			
			entrar();
		}
		
		private void entrar()
		{
			Conexion.iniciarConexion(acceso.getTxtUser().getText(), acceso.getTxtPass().getText(), acceso.getTxtIP().getText(), acceso.getTxtPort().getText());
			if (!Conexion.getConexion().getEstado())
			{
				JOptionPane.showMessageDialog(new JFrame(), "Usuario o contraseña equivocadas", "Error de Conexion", JOptionPane.ERROR_MESSAGE);
				this.acceso.blanquearCampos();
				acceso.cerrar();
				acceso.mostrarVentana();
			}
			else
			{
				acceso.cerrar();
				this.refrescarTabla();
				this.vista.show();
			}	
		}
		
		private void ventanaAgregarPersona(ActionEvent a) {
			this.ventanaPersona.blanquearCampos();
			if (this.ventanaPersona.getComboBoxPaises() != null || 
					this.ventanaPersona.getComboBoxTipo() != null || this.ventanaPersona.getComboBoxEquipo() != null)
				this.ventanaPersona.removerComboBoxes();
			this.ventanaPersona.mostrarVentana();
			this.llenarPaises();
			this.llenarTipos();
			this.llenarEquipos();
			agregarEntersComboPersona();
		}

		private void ventanaEditarPersona(ActionEvent e) {
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			if (filasSeleccionadas.length != 1 )
				JOptionPane.showMessageDialog(new JFrame(), "Es necesario seleccionar un solo contacto para poder continuar", "Dialog", JOptionPane.ERROR_MESSAGE);
			else
			{
				for (int fila : filasSeleccionadas)
				{
					this.editablePersona = this.personasEnTabla.get(fila);
					this.editable = this.personasEnTabla.get(fila).getIdPersona();
					if (this.ventanaEdicion.getComboBoxPaises() != null || 
							this.ventanaEdicion.getComboBoxTipo() != null || this.ventanaEdicion.getComboBoxEquipo() != null)
						this.ventanaEdicion.removerComboBoxes();
					this.ventanaEdicion.mostrarVentana();
					this.llenarPaisesEdicion();	
					this.llenarTiposEdicion();
					this.llenarEquiposEdicion();
					agregarEntersComboEdicion();
					this.ventanaEdicion.setearCampos(editablePersona, agenda);
				}
			}
		}
		
		private void agregarLocalidad(ActionEvent l) {
			this.ventanaLocalidad.blanquearCampos();
			this.ventanaLocalidad.mostrarVentana();			
		}
		
		private void agregarTipoContacto(ActionEvent t) {
			this.ventanaTipoContacto.blanquearCampos();
			this.ventanaTipoContacto.mostrarVentana();			
		}
		
		private void agregarEquipo(ActionEvent eq) {
			this.ventanaEquipo.blanquearCampos();
			this.ventanaEquipo.mostrarVentana();			
		}
		
		private void guardarLocalidad(ActionEvent gl) 
		{
			guardarLocalidad();
		}
		
		private void guardarLocalidad()
		{
			String pais = this.ventanaLocalidad.getTxtPais().getText();
			String provincia = this.ventanaLocalidad.getTxtProvincia().getText();
			String ciudad = this.ventanaLocalidad.getTxtCiudad().getText();
			String cp = this.ventanaLocalidad.getTxtCP().getText();
			if (verificarLocalidad(pais, provincia, ciudad, cp))
			{
				LocalidadDTO nuevaLocalidad = new LocalidadDTO(0, pais, provincia, ciudad, cp);
				this.agenda.agregarLocalidad(nuevaLocalidad);
				this.ventanaLocalidad.cerrar();	
			}
		}
		
		private void guardarTipoContacto(ActionEvent gt) 
		{
			guardarTipoContacto();
		}
		
		private void guardarTipoContacto()
		{
			String tipo = this.ventanaTipoContacto.getTxtTipoContacto().getText();
			if (tipo==null || tipo.length()<1 || tipo.length()>20)
				JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar con un Tipo de Contacto (máximo 20 caracteres) para poder continuar", "Dialog", JOptionPane.ERROR_MESSAGE);
			else
			{
				TipoContactoDTO nuevoTipo = new TipoContactoDTO(0, tipo);
				this.agenda.agregarTipo(nuevoTipo);
				this.ventanaTipoContacto.cerrar();	
			}
		}
		
		private void guardarEquipo(ActionEvent ge) 
		{
			guardarEquipo();
		}
		
		private void guardarEquipo()
		{
			String equipo = this.ventanaEquipo.getTxtEquipo().getText();
			if (equipo==null || equipo.length()<1 || equipo.length()>45)
				JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar con un Equipo de Fútbol (máximo 45 caracteres) para poder continuar", "Dialog", JOptionPane.ERROR_MESSAGE);
			else
			{
				EquipoDTO nuevoEquipo = new EquipoDTO(0, equipo);
				this.agenda.agregarEquipo(nuevoEquipo);
				this.ventanaEquipo.cerrar();	
			}
		}
		
		private void cancelarLocalidad(ActionEvent cl) {
			this.ventanaLocalidad.cerrar();		
		}
		
		private void cancelarTipoContacto(ActionEvent ct) {
			this.ventanaTipoContacto.cerrar();		
		}
		
		private void cancelarEquipo(ActionEvent ce) 
		{
			this.ventanaEquipo.cerrar();		
		}
		
		private void guardarPersona(ActionEvent p) 
		{
			guardarPersona();
		}
		
		private void guardarPersona()
		{
			String nombre = this.ventanaPersona.getTxtNombre().getText();
			String tel = this.ventanaPersona.getTxtTelefono().getText();
			String email = this.ventanaPersona.getTxtEmail().getText();
			String cumpleanio = this.ventanaPersona.getTxtCumpleanio().getText();
			String calle = this.ventanaPersona.getTxtCalle().getText();
			String numero = this.ventanaPersona.getTxtNumero().getText();
			String piso = this.ventanaPersona.getTxtPiso().getText();
			String dpto = this.ventanaPersona.getTxtDpto().getText();
			String pais = (String) this.ventanaPersona.getComboBoxPaises().getSelectedItem();
			String provincia = (String) this.ventanaPersona.getComboBoxProvincias().getSelectedItem();
			String ciudad = (String) this.ventanaPersona.getComboBoxCiudades().getSelectedItem();
			String cp = (String) this.ventanaPersona.getComboBoxCP().getSelectedItem();
			String tipo = (String) this.ventanaPersona.getComboBoxTipo().getSelectedItem();
			String equipo = (String) this.ventanaPersona.getComboBoxEquipo().getSelectedItem();
			
			if (verificarDatos(nombre, tel, email, cumpleanio) && verificarComboBoxs(pais, provincia, ciudad, cp, tipo, equipo))
			{
				int idLocalidad = this.agenda.obtenerIdLocalidad(pais, provincia, ciudad, cp);
				int idTipo = this.agenda.obtenerIdTipo(tipo);
				int idEquipo = this.agenda.obtenerIdEquipo(equipo);
				PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, email, cumpleanio, idLocalidad, idTipo, idEquipo);
				this.agenda.agregarPersona(nuevaPersona);
				int idPersona = this.agenda.obtenerIdPersona(nombre, tel, email, cumpleanio, idLocalidad, idTipo, idEquipo);
				DomicilioDTO domicilio = new DomicilioDTO(idPersona, calle, numero, piso, dpto);
				this.agenda.agregarDomicilio(domicilio);
				this.refrescarTabla();
				this.ventanaPersona.cerrar();
			}
		}
		
		private void editarPersona(ActionEvent ep) 
		{			
			editarPersona();
		}
		
		private void editarPersona()
		{
			String nombre = this.ventanaEdicion.getTxtNombre().getText();
			String tel = this.ventanaEdicion.getTxtTelefono().getText();
			String email = this.ventanaEdicion.getTxtEmail().getText();
			String cumpleanio= this.ventanaEdicion.getTxtCumpleanio().getText();
			String calle = this.ventanaEdicion.getTxtCalle().getText();
			String numero = this.ventanaEdicion.getTxtNumero().getText();
			String piso = this.ventanaEdicion.getTxtPiso().getText();
			String dpto = this.ventanaEdicion.getTxtDpto().getText();
			String pais = (String) this.ventanaEdicion.getComboBoxPaises().getSelectedItem();
			String provincia = (String) this.ventanaEdicion.getComboBoxProvincias().getSelectedItem();
			String ciudad = (String) this.ventanaEdicion.getComboBoxCiudades().getSelectedItem();
			String cp = (String) this.ventanaEdicion.getComboBoxCP().getSelectedItem();
			String tipo = (String) this.ventanaEdicion.getComboBoxTipo().getSelectedItem();
			String equipo = (String) this.ventanaEdicion.getComboBoxEquipo().getSelectedItem();
			
			if (verificarDatos(nombre, tel, email, cumpleanio) && verificarComboBoxs(pais, provincia, ciudad, cp, tipo, equipo))
			{
				this.agenda.borrarPersona(this.editablePersona);
				
				int idLocalidad = this.agenda.obtenerIdLocalidad(pais, provincia, ciudad, cp);
				int idTipo = this.agenda.obtenerIdTipo(tipo);
				int idEquipo = this.agenda.obtenerIdEquipo(equipo);
				PersonaDTO nuevaPersona = new PersonaDTO(editable, nombre, tel, email, cumpleanio, idLocalidad, idTipo, idEquipo);
				this.agenda.agregarPersona(nuevaPersona);
				int idPersona = this.agenda.obtenerIdPersona(nombre, tel, email, cumpleanio, idLocalidad, idTipo, idEquipo);
				DomicilioDTO domicilio = new DomicilioDTO(idPersona, calle, numero, piso, dpto);
				this.agenda.agregarDomicilio(domicilio);			
				
				this.editable = -1;
				this.editablePersona = null;
				this.refrescarTabla();
				this.ventanaEdicion.cerrar();
			}
		}
		
		private void cancelar(ActionEvent c)
		{
			this.ventanaPersona.cerrar();
			this.agregarActionListeners();
			agregarEntersComboPersona();
		}
		
		private void cancelarEdicion(ActionEvent ce)
		{
			this.editable = -1;
			this.editablePersona = null;
			this.ventanaEdicion.cerrar();
			this.agregarActionListenersEdicion();
			agregarEntersComboEdicion();
		}
		
		private void agregarActionListeners()
		{
			this.ventanaPersona.getComboBoxPaises().addActionListener(new ActionListener() {
				@Override
			    public void actionPerformed(ActionEvent event) {
			        @SuppressWarnings("unchecked")
					JComboBox<String> combo = (JComboBox<String>) event.getSource();
			        String pais = (String) combo.getSelectedItem();
			        llenarProvincias(pais);
			    }
			});
			this.ventanaPersona.getComboBoxProvincias().addActionListener(new ActionListener() {
				@Override
			    public void actionPerformed(ActionEvent event) {
			        @SuppressWarnings("unchecked")
					JComboBox<String> combo = (JComboBox<String>) event.getSource();
			        String provincia = (String) combo.getSelectedItem();
			        llenarCiudades(provincia);
			    }
			});
			this.ventanaPersona.getComboBoxCiudades().addActionListener(new ActionListener() {
				@Override
			    public void actionPerformed(ActionEvent event) {
			        @SuppressWarnings("unchecked")
					JComboBox<String> combo = (JComboBox<String>) event.getSource();
			        String ciudad = (String) combo.getSelectedItem();
			        llenarCP(ciudad);
			    }
			});
		}
		
		private void agregarActionListenersEdicion()
		{
			this.ventanaEdicion.getComboBoxPaises().addActionListener(new ActionListener() {
				@Override
			    public void actionPerformed(ActionEvent event) {
			        @SuppressWarnings("unchecked")
					JComboBox<String> combo = (JComboBox<String>) event.getSource();
			        String pais = (String) combo.getSelectedItem();
			        llenarProvinciasEdicion(pais);
			    }
			});
			this.ventanaEdicion.getComboBoxProvincias().addActionListener(new ActionListener() {
				@Override
			    public void actionPerformed(ActionEvent event) {
			        @SuppressWarnings("unchecked")
					JComboBox<String> combo = (JComboBox<String>) event.getSource();
			        String provincia = (String) combo.getSelectedItem();
			        llenarCiudadesEdicion(provincia);
			    }
			});
			this.ventanaEdicion.getComboBoxCiudades().addActionListener(new ActionListener() {
				@Override
			    public void actionPerformed(ActionEvent event) {
			        @SuppressWarnings("unchecked")
					JComboBox<String> combo = (JComboBox<String>) event.getSource();
			        String ciudad = (String) combo.getSelectedItem();
			        llenarCPEdicion(ciudad);
			    }
			});
		}

		private void mostrarReporte(ActionEvent r) {
			List<PersonaDTO> personas = this.agenda.obtenerAllPersonas();
			this.completarDatos(personas);
			ReporteAgenda reporte = new ReporteAgenda(personas);
			reporte.mostrar();	
		}

		public void borrarPersona(ActionEvent s)
		{
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			}
			
			this.refrescarTabla();
		}
		
		public void inicializar()
		{
			acceso.mostrarVentana();
		}
		
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerAllPersonas();
			this.completarDatos(this.personasEnTabla);
			this.vista.llenarTabla(this.personasEnTabla);
		}
		
		private void completarDatos(List<PersonaDTO> personas)
		{
			for (PersonaDTO p : personas)
			{
				DomicilioDTO d = this.agenda.obtenerDomicilio(p.getIdPersona());
				LocalidadDTO l = this.agenda.obtenerLocalidad(p.getIdLocalidad());
				p.setDireccion(d.getDomicilioImprimir() + l.getLocalidadImprimir());
				p.setDomicilio(d.getDomicilioImprimir());
				p.setLocalidad(l.getLocalidadReporte());
				p.setCp(l.getCp());
				p.setTipo(this.agenda.obtenerTipo(p.getIdTipo()));
				p.setEquipo(this.agenda.obtenerEquipo(p.getIdEquipo()));
			}
		}
		
		private void llenarPaises()
		{
			this.paises = agenda.obtenerPaises();
			this.ventanaPersona.llenarPaises(this.paises);
			this.agregarActionListeners();
			agregarEntersComboPersona();
		}
		
		private void llenarProvincias(String pais)
		{
			this.provincias = agenda.obtenerProvincias(pais);
			this.ventanaPersona.llenarProvincias(this.provincias);
			this.agregarActionListeners();
			agregarEntersComboPersona();
		}

		private void llenarCiudades(String provincias)
		{
			String pais = this.ventanaPersona.getComboBoxPaises().getSelectedItem().toString(); 
			this.ciudades = agenda.obtenerCiudades(pais, provincias);
			this.ventanaPersona.llenarCiudades(this.ciudades);
			this.agregarActionListeners();
			agregarEntersComboPersona();
		}
		
		private void llenarCP(String ciudades)
		{
			String pais = this.ventanaPersona.getComboBoxPaises().getSelectedItem().toString(); 
			String provincia = this.ventanaPersona.getComboBoxProvincias().getSelectedItem().toString(); 
			this.cp = agenda.obtenerCP(pais, provincia, ciudades);
			this.ventanaPersona.llenarCP(this.cp);
			this.agregarActionListeners();
			agregarEntersComboPersona();
		}
		
		private void llenarTipos()
		{
			this.tipos = agenda.obtenerAllTipos();
			this.ventanaPersona.llenarTipos(this.tipos);
		}
		
		private void llenarEquipos()
		{
			this.equipos = agenda.obtenerAllEquipos();
			this.ventanaPersona.llenarEquipos(this.equipos);
		}
		
		private void llenarPaisesEdicion()
		{
			this.paises = agenda.obtenerPaises();
			this.ventanaEdicion.llenarPaises(this.paises);
			this.agregarActionListenersEdicion();
			agregarEntersComboEdicion();
		}
		
		private void llenarProvinciasEdicion(String pais)
		{
			this.provincias = agenda.obtenerProvincias(pais);
			this.ventanaEdicion.llenarProvincias(this.provincias);
			this.agregarActionListenersEdicion();
			agregarEntersComboEdicion();
		}

		private void llenarCiudadesEdicion(String provincias)
		{
			String pais = this.ventanaEdicion.getComboBoxPaises().getSelectedItem().toString(); 
			this.ciudades = agenda.obtenerCiudades(pais, provincias);
			this.ventanaEdicion.llenarCiudades(this.ciudades);
			this.agregarActionListenersEdicion();
			agregarEntersComboEdicion();
		}
		
		private void llenarCPEdicion(String ciudades)
		{
			String pais = this.ventanaEdicion.getComboBoxPaises().getSelectedItem().toString(); 
			String provincia = this.ventanaEdicion.getComboBoxProvincias().getSelectedItem().toString(); 
			this.cp = agenda.obtenerCP(pais, provincia, ciudades);
			this.ventanaEdicion.llenarCP(this.cp);
			this.agregarActionListenersEdicion();
			agregarEntersComboEdicion();
		}
		
		private void llenarTiposEdicion()
		{
			this.tipos = agenda.obtenerAllTipos();
			this.ventanaEdicion.llenarTipos(this.tipos);
		}
		
		private void llenarEquiposEdicion()
		{
			this.equipos = agenda.obtenerAllEquipos();
			this.ventanaEdicion.llenarEquipos(this.equipos);
		}
		
		private boolean verificarDatos(String nombre, String tel, String email, String cumpleanio)
		{
			if (nombre==null || nombre.length()<1 || nombre.length()>45) 
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar el Nombre/s y Apellido (máximo 45 caracteres) para poder continuar", "Error Nombre/s y Apellido", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			
			if (tel==null || tel.length()<1 || tel.length()>20) 
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar el Teléfono (máximo 20 caracteres) para poder continuar", "Error Teléfono", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			
			if (email==null || email.length()<1 || email.length()>30) 
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar el Email (máximo 30 caracteres) para poder continuar", "Error Email", JOptionPane.ERROR_MESSAGE);
				return false;
			}			
			try
			{
				@SuppressWarnings("unused")
				LocalDate c = LocalDate.parse(cumpleanio);
			}
			catch (DateTimeParseException e)
			{
				System.out.println(e);
				JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar Día y Mes con una fecha válida para poder continuar", "Dialog", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			return true;			
		}
		
		private boolean verificarComboBoxs(String pais, String provincia, String ciudad, String cp, String tipo, String equipo)
		{
			if (!verificarLocalidad(pais, provincia, ciudad, cp))
				return false;
			if (tipo==null || tipo.length()<1)
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario seleccionar un Tipo de Contacto para poder continuar", "Error Tiopo de Contacto", JOptionPane.ERROR_MESSAGE);
			    return false;
			}
			
			if (equipo==null || equipo.length()<1)
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario seleccionar un Equipo de Fútbol para poder continuar", "Error Equipo de Fútbol", JOptionPane.ERROR_MESSAGE);
			    return false;
			}
			
			return true;
		}
		
		private boolean verificarLocalidad(String pais, String provincia, String ciudad, String cp)
		{
			if (pais==null || pais.length()<1 || pais.length()>45) 
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar un País (máximo 45 caracteres) para poder continuar", "Error Localidad", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			if (provincia==null || provincia.length()<1 || provincia.length()>45)
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar una Provincia (máximo 45 caracteres) para poder continuar", "Error Localidad", JOptionPane.ERROR_MESSAGE);
			    return false;
			}
			if (ciudad==null || ciudad.length()<1 || ciudad.length()>45)
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar una Ciudad (máximo 45 caracteres) para poder continuar", "Error Localidad", JOptionPane.ERROR_MESSAGE);
			    return false;
			}
			if (cp==null || cp.length()<1 || cp.length()>10)
			{
			    JOptionPane.showMessageDialog(new JFrame(), "Es necesario completar un Código Postal (máximo 10 caracteres) para poder continuar", "Error Localidad", JOptionPane.ERROR_MESSAGE);
			    return false;
			}
			return true;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) { }
		
}
