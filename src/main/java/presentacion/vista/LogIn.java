package presentacion.vista;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class LogIn extends JFrame
{
	private static final long serialVersionUID = 1L;
	private static LogIn INSTANCE;
	private JPanel contentPane;
	private JTextField txtUser;
	private JPasswordField txtPass;
	private JTextField txtIP;
	private JTextField txtPort;
	private JButton btnEntrar;
	public static LogIn getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new LogIn(); 	
			return new LogIn();
		}
		else
			return INSTANCE;
	}

	private LogIn() 
	{
		super();
		
		this.setTitle("Autenticación");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 330, 230);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.contentPane.setLayout(null);
				
		JLabel lblUser = new JLabel("Usuario:");
		lblUser.setBounds(10, 12, 68, 15);
		this.contentPane.add(lblUser);
		
		txtUser = new JTextField();
		txtUser.setBounds(128, 10, 176, 20);
		contentPane.add(txtUser);
		txtUser.setColumns(10);
		
		JLabel lblPass = new JLabel("Contraseña:");
		lblPass.setBounds(10, 50, 100, 15);
		this.contentPane.add(lblPass);
		
		txtPass = new JPasswordField();
		txtPass.setBounds(128, 48, 176, 20);
		txtPass.setEchoChar('*');
		contentPane.add(txtPass);
		txtPass.setColumns(10);
		
		JLabel lblIP = new JLabel("Dirección IP:");
		lblIP.setBounds(40, 100, 100, 15);
		this.contentPane.add(lblIP);
		
		txtIP = new JTextField();
		txtIP.setBounds(12, 120, 160, 20);
		contentPane.add(txtIP);
		txtIP.setColumns(10);
		txtIP.setText("localhost");
		
		JLabel lblPort = new JLabel("Puerto:");
		lblPort.setBounds(215, 100, 68, 15);
		this.contentPane.add(lblPort);
		
		txtPort = new JTextField();
		txtPort.setBounds(210, 120, 60, 20);
		contentPane.add(txtPort);
		txtPort.setColumns(10);
		txtPort.setText("3306");
		
		this.btnEntrar = new JButton("Entrar");
		this.btnEntrar.setBounds(195, 160, 110, 25);
		this.contentPane.add(btnEntrar);
			
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public void blanquearCampos() 
	{
		this.txtUser.setText(null);
		this.txtPass.setText(null);
	}
	
	public void cerrar()
	{
		this.dispose();
	}
	
	public JTextField getTxtUser()
	{
		return this.txtUser;
	}
	
	public JTextField getTxtPass()
	{
		return this.txtPass;
	}
	
	public JTextField getTxtIP()
	{
		return this.txtIP;
	}
	
	public JTextField getTxtPort()
	{
		return this.txtPort;
	}
	
	public ArrayList<JTextField> getTxtVentana()
	{
		ArrayList<JTextField> textos = new ArrayList<JTextField>();
		textos.add(txtUser);
		textos.add(txtPass);
		textos.add(txtIP);
		textos.add(txtPort);
		return textos;
	}
	
	public JButton getBtnEntrar() 
	{
		return this.btnEntrar;
	}	
	
}

