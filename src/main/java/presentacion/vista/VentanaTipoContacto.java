package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaTipoContacto extends JFrame
{
	private static final long serialVersionUID = 1L;
	private static VentanaTipoContacto INSTANCE;
	private JPanel contentPane;
	private JTextField txtTipoContacto;
	private JButton btnAgregarTipoContacto;	
	private JButton btnCancelar;
		
	public static VentanaTipoContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaTipoContacto(); 	
			return new VentanaTipoContacto();
		}
		else
			return INSTANCE;
	}

	private VentanaTipoContacto() 
	{
		super();
		
		this.setTitle("Agregar Tipo de Contacto");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 300, 140);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.contentPane.setLayout(null);
				
		JLabel lblTipoContacto = new JLabel("Nuevo Tipo de Contacto:");
		lblTipoContacto.setBounds(65, 10, 200, 15);
		this.contentPane.add(lblTipoContacto);
		
		txtTipoContacto = new JTextField();
		txtTipoContacto.setBounds(50, 40, 200, 20);
		contentPane.add(txtTipoContacto);
		txtTipoContacto.setColumns(10);
		
		this.btnAgregarTipoContacto = new JButton("Agregar");
		this.btnAgregarTipoContacto.setBounds(160, 70, 110, 25);
		this.contentPane.add(btnAgregarTipoContacto);
		
		this.btnCancelar = new JButton("Cancelar");
		this.btnCancelar.setBounds(25, 70, 110, 25);
		this.contentPane.add(btnCancelar);
			
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public void blanquearCampos() 
	{
		this.txtTipoContacto.setText(null);
	}
	
	public void cerrar()
	{
		this.dispose();
	}
	
	public JTextField getTxtTipoContacto()
	{
		return this.txtTipoContacto;
	}
	
	public JButton getBtnAgregarTipoContacto() 
	{
		return this.btnAgregarTipoContacto;
	}
	
	public JButton getBtnCancelar() 
	{
		return this.btnCancelar;
	}
}
