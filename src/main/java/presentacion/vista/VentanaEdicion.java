package presentacion.vista;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import modelo.Agenda;

import javax.swing.JComboBox;

public class VentanaEdicion extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private static VentanaEdicion INSTANCE;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JTextField txtDia;
	private JTextField txtMes;
	private JTextField txtCalle;
	private JTextField txtNumero;
	private JTextField txtPiso;
	private JTextField txtDpto;
	private JComboBox<String> comboBoxPais;
	private JComboBox<String> comboBoxProvincia;
	private JComboBox<String> comboBoxCiudad;
	private JComboBox<String> comboBoxCP;
	private JComboBox<String> comboBoxTipo;
	private JComboBox<String> comboBoxEquipo;
	private JButton btnEditarPersona;	
	private JButton btnCancelar;	
		
	public static VentanaEdicion getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEdicion(); 	
			return new VentanaEdicion();
		}
		else
			return INSTANCE;
	}

	private VentanaEdicion() 
	{
		super();
		
		this.setTitle("Editar Contacto");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 462, 585);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.contentPane.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 152, 14);
		this.contentPane.add(lblNombreYApellido);
		
		this.txtNombre = new JTextField();
		this.txtNombre.setBounds(180, 9, 164, 20);
		this.contentPane.add(txtNombre);
		this.txtNombre.setColumns(10);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 52, 113, 14);
		this.contentPane.add(lblTelfono);
				
		this.txtTelefono = new JTextField();
		this.txtTelefono.setBounds(180, 50, 164, 20);
		this.contentPane.add(txtTelefono);
		this.txtTelefono.setColumns(10);
		
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 78, 70, 15);
		this.contentPane.add(lblEmail);
		
		this.txtEmail = new JTextField();
		this.txtEmail.setBounds(180, 82, 164, 19);
		this.contentPane.add(txtEmail);
		this.txtEmail.setColumns(10);
		
		JLabel lblCumpleaos = new JLabel("Cumpleaños:");
		lblCumpleaos.setBounds(10, 122, 102, 15);
		this.contentPane.add(lblCumpleaos);
		
		JLabel lblDia = new JLabel("Día:");
		lblDia.setBounds(40, 155, 70, 15);
		this.contentPane.add(lblDia);
		
		this.txtDia = new JTextField();
		this.txtDia.setBounds(74, 149, 70, 19);
		this.contentPane.add(txtDia);
		this.txtDia.setColumns(10);
				
		JLabel lblMes = new JLabel("Mes:");
		lblMes.setBounds(162, 151, 70, 15);
		this.contentPane.add(lblMes);
		
		this.txtMes = new JTextField();
		this.txtMes.setBounds(210, 149, 80, 19);
		this.contentPane.add(txtMes);
		this.txtMes.setColumns(10);
		
		JLabel lblDireccion = new JLabel("Direccion:");
		lblDireccion.setBounds(10, 180, 70, 15);
		this.contentPane.add(lblDireccion);
		
		JLabel lblCalle = new JLabel("Calle:");
		lblCalle.setBounds(40, 207, 40, 15);
		this.contentPane.add(lblCalle);
		
		this.txtCalle = new JTextField();
		this.txtCalle.setBounds(87, 205, 114, 19);
		this.contentPane.add(txtCalle);
		this.txtCalle.setColumns(10);
		
		JLabel lblNumero = new JLabel("Numero:");
		lblNumero.setBounds(40, 237, 70, 15);
		this.contentPane.add(lblNumero);
		
		this.txtNumero = new JTextField();
		this.txtNumero.setBounds(118, 235, 114, 19);
		this.contentPane.add(txtNumero);
		this.txtNumero.setColumns(10);
		
		JLabel lblPiso = new JLabel("Piso:");
		lblPiso.setBounds(40, 264, 40, 15);
		this.contentPane.add(lblPiso);
		
		this.txtPiso = new JTextField();
		this.txtPiso.setBounds(87, 264, 114, 19);
		this.contentPane.add(txtPiso);
		this.txtPiso.setColumns(10);
		
		JLabel lblDpto = new JLabel("Dpto:");
		lblDpto.setBounds(40, 294, 40, 15);
		this.contentPane.add(lblDpto);
		
		this.txtDpto = new JTextField();
		this.txtDpto.setBounds(87, 292, 114, 19);
		this.contentPane.add(txtDpto);
		this.txtDpto.setColumns(10);
		
		JLabel lblPais = new JLabel("Pais:");
		lblPais.setBounds(40, 328, 40, 15);
		this.contentPane.add(lblPais);
		
		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(40, 359, 70, 15);
		this.contentPane.add(lblProvincia);

		JLabel lblCiudad = new JLabel("Ciudad:");
		lblCiudad.setBounds(40, 386, 59, 15);
		this.contentPane.add(lblCiudad);
		
		JLabel lblCp = new JLabel("CP:");
		lblCp.setBounds(40, 411, 70, 15);
		contentPane.add(lblCp);
				
		JLabel lblTipoDeContacto = new JLabel("Tipo de contacto:");
		lblTipoDeContacto.setBounds(10, 436, 124, 15);
		contentPane.add(lblTipoDeContacto);
		
		JLabel lblEquipo = new JLabel("Equipo:");
		lblEquipo.setBounds(10, 462, 70, 15);
		contentPane.add(lblEquipo);
		
		this.btnEditarPersona = new JButton("Editar");
		this.btnEditarPersona.setBounds(311, 497, 113, 23);
		this.contentPane.add(btnEditarPersona);
		
		this.btnCancelar = new JButton("Cancelar");
		this.btnCancelar.setBounds(130, 496, 117, 25);
		this.contentPane.add(btnCancelar);
			
		this.setVisible(false);
	}
		
	public void llenarPaises(List<String> paises)
	{
		this.removerComboBoxPais();
		this.removerComboBoxProvincia();
		this.removerComboBoxCiudad();
		this.removerComboBoxCP();
		this.agregarComboBoxPais();
		this.agregarComboBoxProvincia();
		this.agregarComboBoxCiudad();
		this.agregarComboBoxCP();
		for( String p : paises)
			this.comboBoxPais.addItem(p);
	}
	
	public void llenarProvincias(List<String> provincias)
	{
		this.removerComboBoxProvincia();
		this.removerComboBoxCiudad();
		this.removerComboBoxCP();
		this.agregarComboBoxProvincia();
		this.agregarComboBoxCiudad();
		this.agregarComboBoxCP();
		for( String p : provincias)
			this.comboBoxProvincia.addItem(p);
	}
	
	public void llenarCiudades(List<String> ciudades)
	{
		this.removerComboBoxCiudad();
		this.removerComboBoxCP();
		this.agregarComboBoxCiudad();
		this.agregarComboBoxCP();
		for( String c : ciudades)
			this.comboBoxCiudad.addItem(c);
	}
	
	public void llenarCP(List<String> cp)
	{
		this.removerComboBoxCP();
		this.agregarComboBoxCP();
		for(String c : cp)
			this.comboBoxCP.addItem(c);
	}
	
	public void llenarTipos(List<String> tipos)
	{
		this.removerComboBoxTipo();
		this.agregarComboBoxTipo();
		for(String t : tipos)
			this.comboBoxTipo.addItem(t);
	}
	
	public void llenarEquipos(List<String> equipos)
	{
		this.removerComboBoxEquipo();
		this.agregarComboBoxEquipo();
		for(String e : equipos)
			this.comboBoxEquipo.addItem(e);
	}
		
	private void removerComboBoxPais()
	{
		this.contentPane.remove(this.comboBoxPais);
	}
	
	private void removerComboBoxProvincia()
	{
		this.contentPane.remove(this.comboBoxProvincia);
	}
	
	private void removerComboBoxCiudad()
	{
		this.contentPane.remove(this.comboBoxCiudad);
	}
	
	private void removerComboBoxCP()
	{
		this.contentPane.remove(this.comboBoxCP);
	}
	
	private void removerComboBoxTipo()
	{
		this.contentPane.remove(this.comboBoxTipo);
	}
	
	private void removerComboBoxEquipo()
	{
		this.contentPane.remove(this.comboBoxEquipo);
	}
	
	
	private void agregarComboBoxPais() 
	{
		this.comboBoxPais = new JComboBox<String>();
		this.comboBoxPais.setBounds(130, 323, 180, 24);
		this.contentPane.add(this.comboBoxPais);	
		this.comboBoxPais.addItem("");
	}
	
	private void agregarComboBoxProvincia()
	{
		this.comboBoxProvincia = new JComboBox<String>();
		this.comboBoxProvincia.setBounds(130, 354, 180, 24);
		this.contentPane.add(this.comboBoxProvincia);
		this.comboBoxProvincia.addItem("");
	}
	
	private void agregarComboBoxCiudad()
	{
		this.comboBoxCiudad = new JComboBox<String>();
		this.comboBoxCiudad.setBounds(130, 381, 180, 24);
		this.contentPane.add(this.comboBoxCiudad);
		this.comboBoxCiudad.addItem("");
	}
	
	private void agregarComboBoxCP() 
	{
		this.comboBoxCP = new JComboBox<String>();
		this.comboBoxCP.setBounds(130, 411, 180, 24);
		this.contentPane.add(this.comboBoxCP);	
		this.comboBoxCP.addItem("");
	}
	
	private void agregarComboBoxTipo() 
	{
		this.comboBoxTipo = new JComboBox<String>();
		this.comboBoxTipo.setBounds(130, 436, 180, 24);
		this.contentPane.add(this.comboBoxTipo);	
		this.comboBoxTipo.addItem("");
	}
	
	private void agregarComboBoxEquipo() 
	{
		this.comboBoxEquipo = new JComboBox<String>();
		this.comboBoxEquipo.setBounds(130, 462, 180, 24);
		this.contentPane.add(this.comboBoxEquipo);	
		this.comboBoxEquipo.addItem("");
	}
	
	public void mostrarVentana()
	{
		this.agregarComboBoxPais();
		this.agregarComboBoxProvincia();
		this.agregarComboBoxCiudad();
		this.agregarComboBoxCP();
		this.agregarComboBoxTipo();
		this.agregarComboBoxEquipo();
		this.setVisible(true);
	}
	
	public void removerComboBoxes()
	{
		this.removerComboBoxPais();
		this.removerComboBoxProvincia();
		this.removerComboBoxCiudad();
		this.removerComboBoxCP();
		this.removerComboBoxTipo();
		this.removerComboBoxEquipo();
	}

	public void setearCampos(PersonaDTO persona, Agenda agenda) 
	{
		this.txtNombre.setText(persona.getNombre());
		this.txtTelefono.setText(persona.getTelefono());
		String aux = persona.getCumpleanio();
		this.txtDia.setText("" + aux.charAt(8) + aux.charAt(9));
		this.txtMes.setText("" + aux.charAt(5) + aux.charAt(6));
		this.txtEmail.setText(persona.getEmail());
		
		DomicilioDTO dom = agenda.obtenerDomicilio(persona.getIdPersona());
		this.txtCalle.setText(dom.getCalle());
		this.txtNumero.setText(dom.getNumero());
		this.txtPiso.setText(dom.getPiso());
		this.txtDpto.setText(dom.getDpto());
		
		int l = persona.getIdLocalidad();
		LocalidadDTO loc = agenda.obtenerLocalidad(l);
		this.comboBoxPais.setSelectedItem(loc.getPais());
		this.comboBoxProvincia.setSelectedItem(loc.getProvincia());
		this.comboBoxCiudad.setSelectedItem(loc.getCiudad());
		this.comboBoxCP.setSelectedItem(loc.getCp());
		
		int t = persona.getIdTipo();
		String tipo = agenda.obtenerTipo(t);
		this.comboBoxTipo.setSelectedItem(tipo);
		
		int e = persona.getIdEquipo();
		String equipo = agenda.obtenerEquipo(e);
		this.comboBoxEquipo.setSelectedItem(equipo);
	}
	
	public void cerrar()
	{
		this.dispose();
	}
	
	public JTextField getTxtNombre() 
	{
		return this.txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return this.txtTelefono;
	}
	
	public JTextField getTxtEmail() 
	{
		return this.txtEmail;
	}
		
	public JTextField getTxtCumpleanio() 
	{
		return new JTextField ("2020-" + txtMes.getText() + "-" + txtDia.getText());
	}
	
	public JTextField getTxtCalle() 
	{
		return this.txtCalle;
	}
	
	public JTextField getTxtNumero() 
	{
		return this.txtNumero;
	}
	
	public JTextField getTxtPiso() 
	{
		return this.txtPiso;
	}
	
	public JTextField getTxtDpto() 
	{
		return this.txtDpto;
	}
	
	public JTextField getDireccion()
	{
		String calle = txtCalle.getText();
		String num = txtNumero.getText();
		String piso = txtPiso.getText();
		String dpto = txtDpto.getText();
		String pais = comboBoxPais.getSelectedItem().toString();
		String provincia = comboBoxProvincia.getSelectedItem().toString();
		String ciudad = comboBoxCiudad.getSelectedItem().toString();
		return new JTextField ( calle + " " + num + "," + piso + dpto + ","+ ciudad + "," + provincia + "," + pais);
	}
	
	public JComboBox<String> getComboBoxPaises()
	{
		return this.comboBoxPais;
	}
	
	public JComboBox<String> getComboBoxProvincias()
	{		
		return this.comboBoxProvincia;
	}
	
	public JComboBox<String> getComboBoxCiudades() 
	{
		return this.comboBoxCiudad;
	}
	
	public JComboBox<String> getComboBoxCP() 
	{
		return this.comboBoxCP;
	}

	public JComboBox<String> getComboBoxTipo() 
	{
		return this.comboBoxTipo;
	}

	public JComboBox<String> getComboBoxEquipo() 
	{
		return this.comboBoxEquipo;
	}
	
	public JButton getBtnEditarPersona() 
	{
		return this.btnEditarPersona;
	}
	
	public JButton getBtnCancelar() 
	{
		return this.btnCancelar;
	}
	
	public ArrayList<JTextField> getTxtVentana()
	{
		ArrayList<JTextField> textos = new ArrayList<JTextField>();
		textos.add(txtNombre);
		textos.add(txtTelefono);
		textos.add(txtEmail);
		textos.add(txtDia);
		textos.add(txtMes);
		textos.add(txtCalle);
		textos.add(txtNumero);
		textos.add(txtPiso);
		textos.add(txtDpto);
		return textos;
	}
	
	public ArrayList<JComboBox<String>> getComboBoxs()
	{
		ArrayList<JComboBox<String>> combos = new ArrayList<JComboBox<String>>();
		combos.add(comboBoxPais);
		combos.add(comboBoxProvincia);
		combos.add(comboBoxCiudad);
		combos.add(comboBoxCP);
		combos.add(comboBoxTipo);
		combos.add(comboBoxEquipo);
		return combos;
	}
}
