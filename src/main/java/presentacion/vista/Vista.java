package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import dto.PersonaDTO;
import javax.swing.JButton;
import persistencia.conexion.Conexion;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.event.ActionEvent;

public class Vista
{
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnNuevaLocalidad;
	private JButton btnNuevoTipoContacto;
	private JButton btnNuevoEquipo;
	private JButton btnReporte;
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = {"Nombre y apellido", "Telefono", "Email", "Cumple", "Dirección", "Tipo", "Equipo"};

	public Vista() 
	{
		super();
		initialize();
	}

	@SuppressWarnings("serial")
	private void initialize() 
	{
		frame = new JFrame();
		frame.setTitle("Agenda Mangosta");
		frame.setBounds(50, 50, 1245, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 10, 1240, 545);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(0, 0, 1020, 545);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);

		tablaPersonas = new JTable(modelPersonas){
		    @Override
		       public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
		           Component component = super.prepareRenderer(renderer, row, column);
		           int rendererWidth = component.getPreferredSize().width;
		           TableColumn tableColumn = getColumnModel().getColumn(column);
		           tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
		           return component;
		        }
		    };
		tablaPersonas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(1030, 23, 180, 25);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(1030, 58, 180, 25);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(1030, 93, 180, 25);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(1030, 508, 180, 25);
		panel.add(btnReporte);
		
		btnNuevaLocalidad = new JButton("Nueva Localiadad");
		btnNuevaLocalidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNuevaLocalidad.setBounds(1030, 258, 180, 25);
		panel.add(btnNuevaLocalidad);
		
		btnNuevoTipoContacto = new JButton("Nuevo Tipo Contacto");
		btnNuevoTipoContacto.setBounds(1030, 295, 180, 25);
		panel.add(btnNuevoTipoContacto);
		
		btnNuevoEquipo = new JButton("Nuevo Equipo Fútbol");
		btnNuevoEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNuevoEquipo.setBounds(1030, 332, 180, 25);
		panel.add(btnNuevoEquipo);
		
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnNuevaLocalidad() 
	{
		return btnNuevaLocalidad;
	}
	
	public JButton getBtnNuevoTipoContacto() 
	{
		return btnNuevoTipoContacto;
	}
	
	public JButton getBtnNuevoEquipo() 
	{
		return btnNuevoEquipo;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}


	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String email = p.getEmail();
			String cumple = p.getCumpleanioImprimir();
			String dir = p.getDireccion();
			String tipo = p.getTipo();
			String equipo = p.getEquipo();
			Object[] fila = {nombre, tel, email, cumple, dir, tipo, equipo};
			this.getModelPersonas().addRow(fila);
		}
		
	}
}
