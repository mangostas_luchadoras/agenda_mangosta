package presentacion.vista;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaLocalidad extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private static VentanaLocalidad INSTANCE;
	private JPanel contentPane;
	private JTextField txtPais;
	private JTextField txtProvincia;
	private JTextField txtCiudad;
	private JTextField txtCP;
	private JButton btnAgregarLocalidad;	
	private JButton btnCancelar;
		
	public static VentanaLocalidad getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaLocalidad(); 	
			return new VentanaLocalidad();
		}
		else
			return INSTANCE;
	}

	private VentanaLocalidad() 
	{
		super();
		
		this.setTitle("Agregar Localidad");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 414, 200);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.contentPane.setLayout(null);
				
		JLabel lblPais = new JLabel("Pais:");
		lblPais.setBounds(26, 11, 40, 15);
		this.contentPane.add(lblPais);
		
		txtPais = new JTextField();
		txtPais.setBounds(94, 8, 268, 20);
		contentPane.add(txtPais);
		txtPais.setColumns(10);
		
		JLabel lblProvincia = new JLabel("Provincia:");
		lblProvincia.setBounds(26, 45, 70, 15);
		this.contentPane.add(lblProvincia);
		
		txtProvincia = new JTextField();
		txtProvincia.setBounds(94, 39, 268, 20);
		contentPane.add(txtProvincia);
		txtProvincia.setColumns(10);
		
		JLabel lblCiudad = new JLabel("Ciudad:");
		lblCiudad.setBounds(26, 71, 59, 15);
		this.contentPane.add(lblCiudad);
		
		txtCiudad = new JTextField();
		txtCiudad.setBounds(95, 68, 267, 20);
		contentPane.add(txtCiudad);
		txtCiudad.setColumns(10);
		
		JLabel lblCp = new JLabel("CP:");
		lblCp.setBounds(26, 98, 70, 15);
		contentPane.add(lblCp);
		
		txtCP = new JTextField();
		txtCP.setBounds(94, 96, 268, 19);
		contentPane.add(txtCP);
		txtCP.setColumns(10);
		
		this.btnAgregarLocalidad = new JButton("Agregar");
		this.btnAgregarLocalidad.setBounds(249, 140, 113, 23);
		this.contentPane.add(btnAgregarLocalidad);
		
		this.btnCancelar = new JButton("Cancelar");
		this.btnCancelar.setBounds(75, 140, 117, 25);
		this.contentPane.add(btnCancelar);
			
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public void blanquearCampos() 
	{
		this.txtPais.setText(null);
		this.txtProvincia.setText(null);
		this.txtCiudad.setText(null);
		this.txtCP.setText(null);
	}
	
	public void cerrar()
	{
		this.dispose();
	}
	
	public JTextField getTxtPais()
	{
		return this.txtPais;
	}
	
	public JTextField getTxtProvincia()
	{
		return this.txtProvincia;
	}
	
	public JTextField getTxtCiudad()
	{
		return this.txtCiudad;
	}
	
	public JTextField getTxtCP()
	{
		return this.txtCP;
	}
	
	public JButton getBtnAgregarLocalidad() 
	{
		return this.btnAgregarLocalidad;
	}
	
	public JButton getBtnCancelar() 
	{
		return this.btnCancelar;
	}
	
	public ArrayList<JTextField> getTxtVentana()
	{
		ArrayList<JTextField> textos = new ArrayList<JTextField>();
		textos.add(txtPais);
		textos.add(txtProvincia);
		textos.add(txtCiudad);
		textos.add(txtCP);
		return textos;
	}
}
