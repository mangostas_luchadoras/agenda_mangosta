package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class VentanaEquipo extends JFrame
{
	private static final long serialVersionUID = 1L;
	private static VentanaEquipo INSTANCE;
	private JPanel contentPane;
	private JTextField txtEquipo;
	private JButton btnAgregarEquipo;	
	private JButton btnCancelar;
		
	public static VentanaEquipo getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEquipo(); 	
			return new VentanaEquipo();
		}
		else
			return INSTANCE;
	}

	private VentanaEquipo() 
	{
		super();
		
		this.setTitle("Agregar Equipo");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 300, 140);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.contentPane.setLayout(null);
				
		JLabel lblEquipo = new JLabel("Nuevo Equipo de Fútbol:");
		lblEquipo.setBounds(65, 10, 200, 15);
		this.contentPane.add(lblEquipo);
		
		txtEquipo = new JTextField();
		txtEquipo.setBounds(50, 40, 200, 20);
		contentPane.add(txtEquipo);
		txtEquipo.setColumns(10);
		
		this.btnAgregarEquipo = new JButton("Agregar");
		this.btnAgregarEquipo.setBounds(160, 70, 110, 25);
		this.contentPane.add(btnAgregarEquipo);
		
		this.btnCancelar = new JButton("Cancelar");
		this.btnCancelar.setBounds(25, 70, 110, 25);
		this.contentPane.add(btnCancelar);
			
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public void blanquearCampos() 
	{
		this.txtEquipo.setText(null);
	}
	
	public void cerrar()
	{
		this.dispose();
	}
	
	public JTextField getTxtEquipo()
	{
		return this.txtEquipo;
	}
	
	public JButton getBtnAgregarEquipo() 
	{
		return this.btnAgregarEquipo;
	}
	
	public JButton getBtnCancelar() 
	{
		return this.btnCancelar;
	}
}
