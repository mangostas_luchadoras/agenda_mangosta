package modelo;

import java.util.List;
import dto.PersonaDTO;
import dto.LocalidadDTO;
import dto.TipoContactoDTO;
import dto.DomicilioDTO;
import dto.EquipoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DomicilioDAO;
import persistencia.dao.interfaz.EquipoDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;

public class Agenda 
{
	private PersonaDAO persona;
	private DomicilioDAO domicilio;
	private LocalidadDAO localidad;
	private TipoContactoDAO tipo;
	private EquipoDAO equipo;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.domicilio = metodo_persistencia.createDomicilioDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.tipo = metodo_persistencia.createTipoContactoDAO();
		this.equipo = metodo_persistencia.createEquipoDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public PersonaDTO obtenerPersona(int idPersona)
	{
		return this.persona.readPersona(idPersona);		
	}
	
	public int obtenerIdPersona(String nombre, String tel, String email, String cumpleanio, int idLocalidad, int idTipo, int idEquipo)
	{
		return this.persona.readIdPersona(nombre, tel, email, cumpleanio, idLocalidad, idTipo, idEquipo);		
	}
	
	public List<PersonaDTO> obtenerAllPersonas()
	{
		return this.persona.readAll();		
	}
	
	public void agregarLocalidad(LocalidadDTO nuevaLocalidad)
	{
		this.localidad.insert(nuevaLocalidad);
	}
	
	public LocalidadDTO obtenerLocalidad(int idLocalidad) 
	{
		return this.localidad.readLocalidad(idLocalidad);
	}
	
	public int obtenerIdLocalidad(String pais, String provincia, String ciudad, String cp) 
	{
		return this.localidad.readIdLocalidad(pais, provincia, ciudad, cp);
	}
	
	public List<String> obtenerPaises()
	{
		return this.localidad.readPais();
	}
	
	public List<String> obtenerProvincias(String pais)
	{
		return this.localidad.readProvincias(pais);
	}
	
	public List<String> obtenerCiudades(String pais, String provincia)
	{
		return this.localidad.readCiudades(pais, provincia);
	}
	
	public List<String> obtenerCP(String pais, String provincia, String ciudad)
	{
		return this.localidad.readCP(pais, provincia, ciudad);
	}
		
	public void agregarTipo(TipoContactoDTO nuevoTipo)
	{
		this.tipo.insert(nuevoTipo);
	}
	
	public String obtenerTipo(int idTipo)
	{
		return this.tipo.readTipo(idTipo);		
	}
	
	public int obtenerIdTipo(String tipo) 
	{
		return this.tipo.readIdTipo(tipo);
	}
	
	public List<String> obtenerAllTipos()
	{
		return this.tipo.readAllTipos();
	}
	
	public void agregarEquipo(EquipoDTO nuevoEquipo)
	{
		this.equipo.insert(nuevoEquipo);
	}
	
	public String obtenerEquipo(int idEquipo)
	{
		return this.equipo.readEquipo(idEquipo);		
	}
	
	public int obtenerIdEquipo(String equipo) 
	{
		return this.equipo.readIdEquipo(equipo);
	}
	
	public List<String> obtenerAllEquipos()
	{
		return this.equipo.readAllEquipos();
	}
	
	public void agregarDomicilio(DomicilioDTO nuevoDomicilio)
	{
		this.domicilio.insert(nuevoDomicilio);
	}
	
	public DomicilioDTO obtenerDomicilio(int idDomicilio)
	{
		return this.domicilio.readDomicilio(idDomicilio);
	}
}
