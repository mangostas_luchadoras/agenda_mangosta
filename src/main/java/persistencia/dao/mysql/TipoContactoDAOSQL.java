package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoContactoDAO;

public class TipoContactoDAOSQL implements TipoContactoDAO 
{
	private static final String insert = "INSERT INTO tipo_contacto(idTipo, tipo) VALUES(?, ?)";
	private static final String readTipo = "SELECT tipo_contacto.tipo FROM tipo_contacto WHERE tipo_contacto.idtipo = ?";
	private static final String readIdTipo = "SELECT tipo_contacto.idTipo FROM tipo_contacto WHERE tipo_contacto.tipo = ?";
	private static final String readAllTipos = "SELECT DISTINCT tipo_contacto.tipo FROM tipo_contacto";
	
	@Override
	public boolean insert(TipoContactoDTO tipo) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, tipo.getIdTipo());
			statement.setString(2, tipo.getTipo());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public String readTipo(int idTipo) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		String tipo = new String();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readTipo);
			statement.setInt(1, idTipo);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				tipo = getTipo(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipo;
	}
	
	private String getTipo(ResultSet resultSet) throws SQLException
	{
		return resultSet.getString("tipo");
	}
	
	@Override
	public int readIdTipo(String tipo) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		int idTipo = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readIdTipo);
			statement.setString(1, tipo);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				idTipo = getIdTipo(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return idTipo;
	}
	
	private int getIdTipo(ResultSet resultSet) throws SQLException
	{
		return resultSet.getInt("idTipo");
	}

	@Override
	public List<String> readAllTipos() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<String> tipos = new ArrayList<String>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAllTipos);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				tipos.add(getTipo(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipos;
	}

}
