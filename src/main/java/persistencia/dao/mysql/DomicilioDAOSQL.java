package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dto.DomicilioDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DomicilioDAO;

public class DomicilioDAOSQL implements DomicilioDAO 
{
	private static final String insert = "INSERT INTO domicilio(idDomicilio, calle, numero, piso, dpto) VALUES(?, ?, ?, ?, ?)";
	private static final String readDomicilio = "SELECT * FROM domicilio WHERE domicilio.idDomicilio = ?";
			
	@Override
	public boolean insert(DomicilioDTO domicilio) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, domicilio.getIdDomicilio());
			statement.setString(2, domicilio.getCalle());
			statement.setString(3, domicilio.getNumero());
			statement.setString(4, domicilio.getPiso());
			statement.setString(5, domicilio.getDpto());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public DomicilioDTO readDomicilio(int idDomicilio) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		DomicilioDTO domicilio = null;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readDomicilio);
			statement.setInt(1, idDomicilio);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				domicilio = getDomicilio(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return domicilio;
	}
	
	private DomicilioDTO getDomicilio(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idDomicilio");
		String calle = resultSet.getString("calle");
		String numero = resultSet.getString("numero");
		String piso = resultSet.getString("piso");
		String dpto = resultSet.getString("dpto");
		
		return new DomicilioDTO(id, calle, numero, piso, dpto);
	}

}
