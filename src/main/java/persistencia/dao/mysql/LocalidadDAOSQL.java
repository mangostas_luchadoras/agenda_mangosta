package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import dto.LocalidadDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.LocalidadDAO;

public class LocalidadDAOSQL implements LocalidadDAO 
{
	private static final String insert = "INSERT INTO localidad(idLocalidad, pais, provincia, ciudad, cp) VALUES(?, ?, ?, ?, ?)";
	private static final String readLocalidad = "SELECT * FROM localidad WHERE localidad.idLocalidad = ?";
	private static final String readIdLocalidad = "SELECT idLocalidad FROM localidad WHERE pais = ? AND provincia = ? AND ciudad = ? AND cp = ?";
	private static final String readPais = "SELECT DISTINCT pais FROM localidad";
	private static final String readProvincia = "SELECT DISTINCT provincia FROM localidad WHERE pais = ?";
	private static final String readCiudad = "SELECT DISTINCT ciudad FROM localidad WHERE pais = ? AND provincia = ?";
	private static final String readCP = "SELECT DISTINCT cp FROM localidad WHERE pais = ? AND provincia = ? AND ciudad = ?";
	
	@Override
	public boolean insert(LocalidadDTO localidad) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, localidad.getIdLocalidad());
			statement.setString(2, localidad.getPais());
			statement.setString(3, localidad.getProvincia());
			statement.setString(4, localidad.getCiudad());
			statement.setString(5, localidad.getCp());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public LocalidadDTO readLocalidad(int idLocalidad) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		LocalidadDTO localidad = null;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readLocalidad);
			statement.setInt(1, idLocalidad);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				localidad = getLocalidadDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return localidad;
	}
	
	private LocalidadDTO getLocalidadDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idLocalidad");
		String pais = resultSet.getString("Pais");
		String provincia = resultSet.getString("Provincia");
		String ciudad = resultSet.getString("Ciudad");
		String cp = resultSet.getString("CP");
		return new LocalidadDTO(id, pais, provincia, ciudad, cp);
	} 
	
	@Override
	public int readIdLocalidad(String pais, String provincia, String ciudad, String cp) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		int id = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readIdLocalidad);
			statement.setString(1, pais);
			statement.setString(2, provincia);
			statement.setString(3, ciudad);
			statement.setString(4, cp);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				id = getIdLocalidad(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return id;
	}
	
	private int getIdLocalidad(ResultSet resultSet) throws SQLException
	{
		return resultSet.getInt("idLocalidad");
	}
	
	@Override
	public List<String> readPais() 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<String> paises = new ArrayList<String>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readPais);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				paises.add(getPais(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return paises;
	}
	
	private String getPais(ResultSet resultSet) throws SQLException
	{
		return resultSet.getString("pais");
	}

	@Override
	public List<String> readProvincias(String pais) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<String> provincias = new ArrayList<String>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readProvincia);
			statement.setString(1, pais);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				provincias.add(getProvincia(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return provincias;
	}
	
	private String getProvincia(ResultSet resultSet) throws SQLException
	{
		return resultSet.getString("provincia");
	}

	@Override
	public List<String> readCiudades(String pais, String provincia) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<String> ciudades = new ArrayList<String>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readCiudad);
			statement.setString(1, pais);
			statement.setString(2, provincia);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				ciudades.add(getCiudad(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return ciudades;
	}
	
	private String getCiudad(ResultSet resultSet) throws SQLException
	{
		return resultSet.getString("ciudad");
	}
	
	@Override
	public List<String> readCP(String pais, String provincia, String ciudad) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<String> cp = new ArrayList<String>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readCP);
			statement.setString(1, pais);
			statement.setString(2, provincia);
			statement.setString(3, ciudad);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				cp.add(getCP(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return cp;
	}
	
	private String getCP(ResultSet resultSet) throws SQLException
	{
		return resultSet.getString("cp");
	}

}
