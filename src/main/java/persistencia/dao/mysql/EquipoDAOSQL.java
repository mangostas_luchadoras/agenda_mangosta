package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.EquipoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.EquipoDAO;

public class EquipoDAOSQL implements EquipoDAO 
{
	private static final String insert = "INSERT INTO equipo(idEquipo, nombre) VALUES(?, ?)";
	private static final String readEquipo = "SELECT equipo.nombre FROM equipo WHERE equipo.idequipo = ?";
	private static final String readIdEquipo = "SELECT equipo.idEquipo FROM equipo WHERE equipo.nombre = ?";
	private static final String readAllEquipos = "SELECT DISTINCT equipo.nombre FROM equipo";
	
	@Override
	public boolean insert(EquipoDTO equipo) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, equipo.getIdEquipo());
			statement.setString(2, equipo.getNombre());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public String readEquipo(int idEquipo) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		String nombre = new String();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readEquipo);
			statement.setInt(1, idEquipo);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				nombre = getNombre(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return nombre;
	}
	
	private String getNombre(ResultSet resultSet) throws SQLException
	{
		return resultSet.getString("nombre");
	}
	
	@Override
	public int readIdEquipo(String nombre) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		int id = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readIdEquipo);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				id = getIdEquipo(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return id;
	}
	
	private int getIdEquipo(ResultSet resultSet) throws SQLException
	{
		return resultSet.getInt("idEquipo");
	}

	@Override
	public List<String> readAllEquipos() 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<String> nombres = new ArrayList<String>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAllEquipos);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				nombres.add(getNombre(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return nombres;
	}

}
