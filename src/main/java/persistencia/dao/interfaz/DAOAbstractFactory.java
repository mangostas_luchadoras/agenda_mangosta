package persistencia.dao.interfaz;


public interface DAOAbstractFactory 
{
	public PersonaDAO createPersonaDAO();
	
	public LocalidadDAO createLocalidadDAO();
	
	public EquipoDAO createEquipoDAO();
	
	public TipoContactoDAO createTipoContactoDAO();
	
	public DomicilioDAO createDomicilioDAO();
}
