package persistencia.dao.interfaz;

import dto.DomicilioDTO;

public interface DomicilioDAO 
{
	public boolean insert(DomicilioDTO domicilio);
	
	public DomicilioDTO readDomicilio(int idDomicilio);
}
