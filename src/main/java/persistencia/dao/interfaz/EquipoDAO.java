package persistencia.dao.interfaz;

import java.util.List;
import dto.EquipoDTO;

public interface EquipoDAO 
{
	public boolean insert(EquipoDTO equipo);
	
	public String readEquipo(int idEquipo);
	
	public int readIdEquipo(String nombre);
	
	public List<String> readAllEquipos();	
}
