package persistencia.dao.interfaz;

import java.util.List;

import dto.PersonaDTO;

public interface PersonaDAO 
{
	
	public boolean insert(PersonaDTO persona);

	public boolean delete(PersonaDTO persona_a_eliminar);
	
	public PersonaDTO readPersona(int idPersona);
	
	public int readIdPersona(String nombre, String telefono, String email, String cumpleanio, int idLocalidad, int idTipo, int idEquipo);
	
	public List<PersonaDTO> readAll();
}
