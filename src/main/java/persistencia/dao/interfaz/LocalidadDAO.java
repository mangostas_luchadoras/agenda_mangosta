package persistencia.dao.interfaz;

import java.util.List;
import dto.LocalidadDTO;

public interface LocalidadDAO 
{
	public boolean insert(LocalidadDTO localidad);
	
	public LocalidadDTO readLocalidad(int idLocalidad);
	
	public int readIdLocalidad(String pais, String provincia, String ciudad, String cp);
	
	public List<String> readPais();
	
	public List<String> readProvincias(String pais);
	
	public List<String> readCiudades(String pais, String ciudad);
	
	public List<String> readCP(String pais, String provincia, String ciudad);
}
