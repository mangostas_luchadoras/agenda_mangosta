package persistencia.dao.interfaz;

import java.util.List;
import dto.TipoContactoDTO;

public interface TipoContactoDAO 
{
	public boolean insert(TipoContactoDTO tipo);
	
	public String readTipo(int idTipo);
	
	public int readIdTipo(String nombre);
	
	public List<String> readAllTipos();	
}
