package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;

public class Conexion 
{
	public static Conexion instancia;
	private Connection connection;
	private String user;
	private String pass;
	private String ip;
	private String port;
	private boolean estado = false;
	private Logger log = Logger.getLogger(Conexion.class);
	
	private Conexion()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver"); // quitar si no es necesario
			this.connection = DriverManager.getConnection("jdbc:mysql://" + ip + ":"+ port +"/grupo_3", user, pass);
			this.connection.setAutoCommit(false);
			log.info("Conexión exitosa");
			this.estado = true;
		}
		catch(Exception e)
		{
			log.error("Conexión fallida", e);
			this.estado = false;
		}
	}
	
	private Conexion(String usuario, String contrasenia, String IP, String puerto)
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver"); // quitar si no es necesario
			this.connection = DriverManager.getConnection("jdbc:mysql://" + IP + ":" + puerto + "/grupo_3", usuario, contrasenia);
			this.connection.setAutoCommit(false);
			log.info("Conexión exitosa");
			this.user = usuario;
			this.pass = contrasenia;
			this.ip = IP;
			this.port = puerto;
			this.estado = true;
		}
		catch(Exception e)
		{
			log.error("Conexión fallida", e);
			this.estado = false;
		}
	}
	
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion();
		}
		return instancia;
	}
	
	public static void iniciarConexion(String usuario, String contrasenia, String IP, String puerto)   
	{		
		instancia = new Conexion(usuario, contrasenia, IP, puerto);
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public boolean getEstado()
	{
		return estado;
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}
}
