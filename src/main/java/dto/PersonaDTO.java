package dto;

public class PersonaDTO 
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private String email;
	private String cumpleanio;
	private String direccion;
	private String domicilio;
	private String localidad;
	private String cp;
	private String tipo;
	private String equipo;
	private int idLocalidad;
	private int idTipo;
	private int idEquipo;
	
	public PersonaDTO(int idPersona, String nombre, String telefono, String email, String cumpleanio, int idLocalidad, int idTipo, int idEquipo)
	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.email = email;
		this.cumpleanio = cumpleanio;
		this.direccion = "";
		this.domicilio = "";
		this.localidad = "";
		this.cp = "";
		this.tipo = "";
		this.equipo = "";
		this.idLocalidad = idLocalidad;
		this.idTipo = idTipo;
		this.idEquipo= idEquipo;
	}
	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}

	public String getEmail() 
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getCumpleanioImprimir() 
	{
		String dia = new String("" + cumpleanio.charAt(8) + cumpleanio.charAt(9));
		String mes = new String("" + cumpleanio.charAt(5) + cumpleanio.charAt(6));
		return new String(dia + "/" + mes);
	}

	public String getCumpleanio()
	{
		return this.cumpleanio;
	}
	
	public void setCumpleanio(String cumpleanio) 
	{
		this.cumpleanio = cumpleanio;
	}

	public String getDireccion() 
	{
		return direccion;
	}

	public void setDireccion(String direccion) 
	{
		this.direccion = direccion;
	}
	
	public String getDomicilio() 
	{
		return domicilio;
	}

	public void setDomicilio(String domicilio) 
	{
		this.domicilio = domicilio;
	}

	public String getLocalidad() 
	{
		return localidad;
	}

	public void setLocalidad(String localidad) 
	{
		this.localidad = localidad;
	}

	public String getCp() 
	{
		return cp;
	}

	public void setCp(String cp) 
	{
		this.cp = cp;
	}

	public String getTipo() 
	{
		return tipo;
	}

	public void setTipo(String tipo) 
	{
		this.tipo = tipo;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String equipo) 
	{
		this.equipo = equipo;
	}

	public int getIdLocalidad()
	{
		return idLocalidad;
	}

	public void setIdLocalidad(int idLocalidad) 
	{
		this.idLocalidad = idLocalidad;
	}

	public int getIdTipo() 
	{
		return idTipo;
	}

	public void setIdTipo(int idTipo)
	{
		this.idTipo = idTipo;
	}

	public int getIdEquipo() 
	{
		return idEquipo;
	}

	public void setIdEquipo(int idEquipo)
	{
		this.idEquipo = idEquipo;
	}

	
}
