package dto;

public class DomicilioDTO 
{
	private int idDomicilio;
	private String calle;
	private String numero;
	private String piso;
	private String dpto;
	
	public DomicilioDTO(int idDomicilio, String calle, String numero, String piso, String dpto) 
	{
		this.idDomicilio = idDomicilio;
		this.calle = calle;
		this.numero = numero;
		this.piso = piso;
		this.dpto = dpto;
	}

	public int getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(int idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}
	
	public String getDomicilioImprimir()
	{
		return  calle + " " + numero + "," + piso + dpto ;
	}
	
}
