package dto;

public class LocalidadDTO 
{
	private int idLocalidad;
	private String pais;
	private String provincia;
	private String ciudad;
	private String cp;
	
	public LocalidadDTO(int idLocalidad, String pais, String provincia, String ciudad, String cp)
	{
		this.idLocalidad = idLocalidad;
		this.pais = pais;
		this.provincia = provincia;
		this.ciudad = ciudad;
		this.cp = cp;
	}
	
	public int getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(int idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}
	
	public String getLocalidadImprimir()
	{
		 return "," + ciudad + "," + provincia + "," + pais + ". CP " + cp;
	}
	
	public String getLocalidadReporte()
	{
		return "CP " + cp + " - " + ciudad + "," + provincia + "," + pais + ".";
	}
}
